# Unity 拉取海康威视网络摄像头画面所需的DLL和脚本

## 简介
本仓库提供了在Unity中拉取海康威视网络摄像头画面所需的DLL文件和相关脚本。通过使用这些资源，开发者可以轻松地在Unity项目中集成海康威视网络摄像头的视频流。

## 参考文章
本资源文件的实现参考了以下文章：
[Unity中使用海康威视网络摄像头](https://blog.csdn.net/weixin_44568736/article/details/132186279?csdn_share_tail=%7B%22type%22%3A%22blog%22%2C%22rType%22%3A%22article%22%2C%22rId%22%3A%22132186279%22%2C%22source%22%3A%22weixin_44568736%22%7D)

## 文件结构
- `DLLs/`：包含所需的DLL文件。
- `Scripts/`：包含用于拉取和显示摄像头画面的Unity脚本。

## 使用方法
1. **下载仓库**：
   ```bash
   git clone https://github.com/your-repo-url.git
   ```

2. **导入DLL文件**：
   - 将`DLLs/`目录中的所有DLL文件复制到你的Unity项目的`Assets/Plugins`目录下。

3. **导入脚本**：
   - 将`Scripts/`目录中的所有脚本文件复制到你的Unity项目的`Assets/Scripts`目录下。

4. **配置摄像头**：
   - 在Unity中创建一个空物体，并将摄像头拉取脚本附加到该物体上。
   - 根据需要配置脚本中的摄像头IP地址、端口、用户名和密码等参数。

5. **运行项目**：
   - 运行Unity项目，你应该能够看到海康威视网络摄像头的实时画面。

## 注意事项
- 确保你的海康威视网络摄像头已经正确配置并可以正常访问。
- 根据你的具体需求，可能需要对脚本进行进一步的修改和优化。

## 许可证
本项目采用MIT许可证。有关更多信息，请参阅[LICENSE](LICENSE)文件。

## 贡献
欢迎提交问题和拉取请求，共同完善本项目。

## 联系
如有任何问题或建议，请通过[issues](https://github.com/your-repo-url/issues)页面联系我们。

---

希望本资源对你在Unity中集成海康威视网络摄像头有所帮助！